
Ansible Erlang Provisioning
======================
This project provides ansible cinfiguration to set up docker and erlang on an inventory of servers.  It uses Vagrant to provision 3 servers that will then have the ansible configuration added to them.  



## Start the 3 servers

```
Vagrant up
```


## Run the Ansible provisioning 
```
ansible-playbook -e env=local  -i ./ansible/hosts/local ./ansible/erlang.yml -vvvvv

```


## To change the servers 
```
Amend the inventory file, ./ansible/hosts/local

```